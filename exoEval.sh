#!/bin/bash

# To read the name of the directory
function chooseDirName(){
    read -p "Please enter a directory name : " dirname
}

# Checks if the name of the directory exists and if it's a directory
# If it's not a directory, enter a dirname again.
function verifDirName(){
    while [[ ! -d $dirname ]] 
        do 
            echo "This directory doesn't exist!"
            chooseDirName
        done
}

# It replaces spaces in filenames by _ 
function replaceSpaces(){
    for f in $dirname/*
        do 
            mv "$f" "${f// /_}"
        done
}

# Create and fill the log file
function displayLogs(){
    date=$(date)
    res=$(find . -type f | rev | cut -d. -f1 | rev  | tr '[:upper:]' '[:lower:]' | sort | uniq --count | sort -rn)
    echo "The folder to sort is : "$dirname"" >> datalogs.txt
    echo "The folder was sorted at "$date"" >> datalogs.txt
    echo $res >> datalogs.txt
}

# Fisrtly, lists the filenames in the directory and go in the directory.
# Secondly, make a directory for every extension and move every files into. 
function process(){
    dir=$( ls $dirname )
    cd $dirname
    for file in $dir
        do
            ext=$( echo $file | rev | cut -d. -f1 | rev)
            mkdir -p $ext
            mv $file $ext
        done
}

# Main function of the script.
function main(){
    if [[ $# -ne 0 ]] ; then
        dirname=$1
    else
        chooseDirName
    fi
    verifDirName
    replaceSpaces
    process
    displayLogs
}

main $@