# Le classeur de rangement

## Consigne

Réaliser un script bash qui va classer dans des sous répertoire
tous les fichiers d'un répertoire selon leurs extensions.
Le script prend en argument le nom du répertoire cible
(ou demande la saisie du dossier cible à trier).

bonus : faire un fichier de log qui affiche :
    - le nom du dossier trier
    - la date à laquelle il a ete trier
    - le resultat du tri (combien de fichier de chaque type)

remarque : code doit etre documenté ! 